import IBitcoinQuotation from 'interfaces/models/bitcoinQuotation';
import IBitcoinVolume from 'interfaces/models/bitcoinVolume';
import IBitcoinHistory from 'interfaces/models/bitcoinHistory';
import IBitcoinPurchase from 'interfaces/models/bitcoinPurchase';
import IBitcoinSell from 'interfaces/models/bitcoinSell';
import IBitcoinPosition from 'interfaces/models/bitcoinPosition';

import { Observable } from 'rxjs';

import apiService, { ApiService } from './api';

export class BitcoinService {
  constructor(private apiService: ApiService) {}

  public quotation(): Observable<IBitcoinQuotation> {
    return this.apiService.get('/btc/price');
  }

  public volume(): Observable<IBitcoinVolume> {
    return this.apiService.get('/volume');
  }

  public history(): Observable<IBitcoinHistory[]> {
    return this.apiService.get('/history');
  }

  public purchase(model: Partial<IBitcoinPurchase>): Observable<IBitcoinPurchase> {
    return this.apiService.post('/btc/purchase', model);
  }

  public sell(model: Partial<IBitcoinSell>): Observable<IBitcoinSell> {
    return this.apiService.post('/btc/sell', model);
  }

  public position(): Observable<IBitcoinPosition[]> {
    return this.apiService.get('/btc');
  }
}

const bitcoinService = new BitcoinService(apiService);
export default bitcoinService;
