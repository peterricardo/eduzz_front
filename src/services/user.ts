import cache from 'helpers/rxjs-operators/cache';
import IUser from 'interfaces/models/user';
import IUserRole from 'interfaces/models/userRole';
import IUserBalance from 'interfaces/models/userBalance';
import IDeposit from 'interfaces/models/deposit';
import IExtract from 'interfaces/models/extract';

import { IPaginationParams, IPaginationResponse } from 'interfaces/pagination';
import { Observable } from 'rxjs';

import apiService, { ApiService } from './api';

export class UserService {
  constructor(private apiService: ApiService) {}

  public list(params: IPaginationParams): Observable<IPaginationResponse<IUser>> {
    return this.apiService.get('/user', params);
  }

  public current(): Observable<IUser> {
    return this.apiService.get('/user/current');
  }

  public roles(refresh: boolean = false): Observable<IUserRole[]> {
    return this.apiService.get('/user/roles').pipe(cache('user-service-roles', { refresh }));
  }

  public save(model: Partial<IUser>): Observable<IUser> {
    return this.apiService.post('/user', model);
  }

  public delete(id: number): Observable<void> {
    return this.apiService.delete(`/user/${id}`);
  }

  public balance(): Observable<IUserBalance> {
    return this.apiService.get('/account/balance');
  }

  public deposit(model: Partial<IDeposit>): Observable<IDeposit> {
    return this.apiService.post('/account/deposit', model);
  }

  public extract(): Observable<IExtract[]> {
    return this.apiService.get('/extract');
  }
}

const userService = new UserService(apiService);
export default userService;
