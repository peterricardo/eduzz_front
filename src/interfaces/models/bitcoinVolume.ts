export default interface IBitcoinVolume {
  sell: number;
  buy: number;
}
