export default interface IBitcoinPosition {
  id: string;
  purchasedAmount: number;
  purchasedPrice: number;
  purchasedBtcAmount: number;
  currentBtcPrice: number;
  currentBtcAmount: number;
  variation: number;
  sellAmount: number;
  purchasedDate: Date;
}
