export default interface IBitcoinQuotation {
  sell: number;
  buy: number;
}
