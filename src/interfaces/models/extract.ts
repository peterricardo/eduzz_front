export default interface IExtract {
  id: string;
  type: string;
  value: number;
  createdAt: Date;
}
