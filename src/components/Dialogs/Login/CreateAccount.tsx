import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import LinearProgress from '@material-ui/core/LinearProgress';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import TextField from 'components/Shared/Fields/Text';
import { logError } from 'helpers/rxjs-operators/logError';
import { useFormikObservable } from 'hooks/useFormikObservable';
import React, { memo, MouseEvent } from 'react';
import { switchMap } from 'rxjs/operators';
import * as Rx from 'rxjs';
import authService from 'services/auth';
import * as yup from 'yup';

interface IProps {
  onCancel: (e: MouseEvent<HTMLElement>) => void;
  onComplete: (email: string, password: string) => Rx.Observable<void>;
}

const validationSchema = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().required().email(),
  password: yup.string().required()
});

const useStyle = makeStyles({
  buttons: {
    justifyContent: 'space-between'
  }
});

const CreateAccount = memo((props: IProps) => {
  const classes = useStyle(props);

  const formik = useFormikObservable({
    initialValues: { email: '', name: '', password: '' },
    validationSchema,
    onSubmit(model) {
      const { email, name, password } = model;
      return authService.createAccount(email, name, password).pipe(
        switchMap(() => props.onComplete(email, password)),
        logError(true)
      );
    }
  });

  return (
    <form noValidate onSubmit={formik.handleSubmit}>
      <Card>
        <CardContent>
          <Typography gutterBottom>Crie sua conta!</Typography>
          <TextField label='Nome' type='string' name='name' formik={formik} />
          <TextField label='Email' type='email' name='email' formik={formik} />
          <TextField label='Senha' type='password' name='password' formik={formik} margin='none' />
        </CardContent>

        <CardActions className={classes.buttons}>
          <Button disabled={formik.isSubmitting} size='small' onClick={props.onCancel}>
            Voltar
          </Button>
          <Button disabled={formik.isSubmitting} color='primary' type='submit'>
            Criar Conta
          </Button>
        </CardActions>

        {formik.isSubmitting && <LinearProgress color='primary' />}
      </Card>
    </form>
  );
});

export default CreateAccount;
