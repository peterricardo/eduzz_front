import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';
import makeStyles from '@material-ui/core/styles/makeStyles';
import logoWhite from 'assets/images/logo.png';
import bgImage from 'assets/images/bg-login.jpg';
import { logError } from 'helpers/rxjs-operators/logError';
import React, { forwardRef, memo, useCallback, useState } from 'react';
import SwipeableViews from 'react-swipeable-views';
import { useObservable } from 'react-use-observable';
import authService from 'services/auth';

import LoginDialogForm from './Form';
import CreateAccount from './CreateAccount';

const useStyle = makeStyles({
  root: {
    minHeight: '100vh',
    minWidth: '100vw',
    position: 'relative',
    background: `url(${bgImage}) no-repeat center`,
    backgroundSize: 'cover'
  },
  container: {
    position: 'absolute',
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    margin: 'auto',
    width: 320,
    height: 470,
    maxWidth: 'calc(100% - 30px)',
    color: 'white'
  },
  logo: {
    textAlign: 'center',
    marginBottom: 10
  },
  logoImage: {
    width: 200,
    maxWidth: '100%',
    maxHeight: 150
  },
  viewContainer: {
    boxSizing: 'border-box',
    padding: '0 10px',
    height: 325
  }
});

const LoginDialog = memo((props: {}) => {
  const classes = useStyle(props);
  const [currentView, setCurrentView] = useState(0);

  const [opened] = useObservable(() => {
    return authService.shouldOpenLogin().pipe(logError());
  }, []);

  const handleChangeView = useCallback((view: number) => () => setCurrentView(view), []);

  const loginAfterCreateAccount = (email: string, password: string) => {
    console.log('pampa');
    return authService.login(email, password).pipe(logError(true));
  };

  return (
    <Dialog
      fullScreen
      disableBackdropClick
      disableEscapeKeyDown
      open={opened || false}
      TransitionComponent={Transition}
    >
      <div className={classes.root}>
        <div className={classes.container}>
          <div className={classes.logo}>
            <img src={logoWhite} className={classes.logoImage} alt='logo' />
          </div>

          <SwipeableViews index={currentView}>
            <div className={classes.viewContainer}>
              <LoginDialogForm onCreateAccount={handleChangeView(1)} />
            </div>
            <div className={classes.viewContainer}>
              <CreateAccount onCancel={handleChangeView(0)} onComplete={loginAfterCreateAccount} />
            </div>
          </SwipeableViews>
        </div>
      </div>
    </Dialog>
  );
});

const Transition = memo(
  forwardRef((props: any, ref: any) => {
    return <Slide direction='up' {...props} ref={ref} />;
  })
);

export default LoginDialog;
