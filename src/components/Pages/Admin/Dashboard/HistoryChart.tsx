import React from 'react';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import Typography from '@material-ui/core/Typography';
import { Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';

import { logError } from 'helpers/rxjs-operators/logError';
import { useObservable } from 'react-use-observable';
import bitcoinService from 'services/bitcoin';

import { dateFormat } from 'formatters/date';

const HistoryChart = () => {
  const [data] = useObservable(() => {
    return bitcoinService.history().pipe(logError());
  }, []);

  const history = data || ([] as typeof data);
  const chartData = history
    .map(h => {
      return {
        hour: dateFormat(h.createdAt, 'HH:MM'),
        compra: h.buy,
        venda: h.sell
      };
    })
    .reverse();

  return (
    <Card>
      <CardContent>
        <Typography gutterBottom variant='h5'>
          Histórico últimas 24h
        </Typography>

        <ResponsiveContainer width='100%' height={200}>
          <LineChart data={chartData}>
            <Tooltip />
            <XAxis dataKey='hour' />
            <YAxis />
            <Line type='monotone' dataKey='compra' stroke='#8884d8' strokeWidth={2} />
            <Line type='monotone' dataKey='venda' stroke='#d884a7' strokeWidth={2} />
          </LineChart>
        </ResponsiveContainer>
      </CardContent>
    </Card>
  );
};

export default HistoryChart;
