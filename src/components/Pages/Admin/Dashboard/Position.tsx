import React, { memo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { logError } from 'helpers/rxjs-operators/logError';
import { useObservable } from 'react-use-observable';
import bitcoinService from 'services/bitcoin';
import IBitcoinPosition from 'interfaces/models/bitcoinPosition';
import { dateFormat } from 'formatters/date';

function createRow(row: IBitcoinPosition) {
  return row;
}

const useStyles = makeStyles({
  card: {
    padding: 15,
    marginTop: 15,
    width: '100%'
  },
  table: {
    minWidth: 650
  }
});

const Position = memo((props: {}) => {
  const classes = useStyles(props);
  const [data] = useObservable(() => {
    return bitcoinService.position().pipe(logError());
  }, []);

  const rows = (data || ([] as typeof data)).map(p => createRow(p));

  console.log(rows);
  return (
    <Card className={classes.card}>
      <Typography gutterBottom variant='h4'>
        Posição dos Investimentos
      </Typography>
      <CardContent>
        <TableContainer component={Paper}>
          <Table className={classes.table} size='small' aria-label='a dense table'>
            <TableHead>
              <TableRow>
                <TableCell>Data da Compra</TableCell>
                <TableCell align='right'>Valor Compra</TableCell>
                <TableCell align='right'>Valor BTC Compra</TableCell>
                <TableCell align='right'>BTC Comprado</TableCell>
                <TableCell align='right'>Estimativa Venda</TableCell>
                <TableCell align='right'>Valor BTC Venda</TableCell>
                <TableCell align='right'>Estimativa BTC Venda</TableCell>
                <TableCell align='right'>Variação</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map(row => (
                <TableRow key={row.id}>
                  <TableCell align='right'>{dateFormat(row.purchasedDate, 'dd/MM/yyyy HH:mm')}</TableCell>
                  <TableCell align='right'>{row.purchasedAmount}</TableCell>
                  <TableCell align='right'>{row.purchasedPrice}</TableCell>
                  <TableCell align='right'>{row.purchasedBtcAmount}</TableCell>
                  <TableCell align='right'>{row.sellAmount}</TableCell>
                  <TableCell align='right'>{row.currentBtcPrice}</TableCell>
                  <TableCell align='right'>{row.currentBtcAmount}</TableCell>
                  <TableCell align='right'>{row.variation}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </CardContent>
    </Card>
  );
});

export default Position;
