import React, { Fragment, memo } from 'react';
import Grid from '@material-ui/core/Grid';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Toolbar from 'components/Layout/Toolbar';

import Balance from 'components/Shared/Balance';
import BitcoinQuotation from 'components/Shared/BitcoinQuotation';
import BitcoinVolume from 'components/Shared/BitcoinVolume';
import HistoryChart from './HistoryChart';
import Position from './Position';

const useStyles = makeStyles({
  marginBottom: {
    marginBottom: 15
  }
});

const DashboardIndexPage = memo((props: {}) => {
  const classes = useStyles(props);

  return (
    <Fragment>
      <Toolbar title='Dashboard' />

      <Grid container spacing={2} className={classes.marginBottom}>
        <Grid container item xs={12} md={6} lg={4}>
          <Balance />
        </Grid>
        <Grid container item xs={12} md={6} lg={4}>
          <BitcoinQuotation />
        </Grid>
        <Grid container item xs={12} md={6} lg={4}>
          <BitcoinVolume />
        </Grid>
      </Grid>
      <HistoryChart />
      <Position />
    </Fragment>
  );
});

export default DashboardIndexPage;
