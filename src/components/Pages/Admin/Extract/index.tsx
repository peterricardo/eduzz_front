import React, { memo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Toolbar from 'components/Layout/Toolbar';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { logError } from 'helpers/rxjs-operators/logError';
import { useObservable } from 'react-use-observable';
import userService from 'services/user';
import IExtract from 'interfaces/models/extract';
import { dateFormat } from 'formatters/date';

function createRow(row: IExtract) {
  return row;
}

function translateType(type: string) {
  switch (type) {
    case 'investment':
      return 'Investimento';
    case 'deposit':
      return 'Depósito';
    case 'liquidation':
      return 'Liquidação';
    default:
      return 'Movimentação';
  }
}
const useStyles = makeStyles({
  card: {
    padding: 15,
    width: '100%'
  },
  table: {
    minWidth: 650
  }
});

const Extract = memo((props: {}) => {
  const classes = useStyles(props);
  const [data] = useObservable(() => {
    return userService.extract().pipe(logError());
  }, []);

  const rows = (data || ([] as typeof data)).map(p => createRow(p));

  return (
    <Card className={classes.card}>
      <Toolbar title='Extrato das Movimentações' />
      <Typography gutterBottom variant='h4'>
        Extrato das Movimentações
      </Typography>
      <CardContent>
        <TableContainer component={Paper}>
          <Table className={classes.table} size='small' aria-label='a dense table'>
            <TableHead>
              <TableRow>
                <TableCell>Data/Hora da Movimentação</TableCell>
                <TableCell>Tipo</TableCell>
                <TableCell>R$ Valor</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map(row => (
                <TableRow key={row.id}>
                  <TableCell>{dateFormat(row.createdAt, 'dd/MM/yyyy HH:mm')}</TableCell>
                  <TableCell>{translateType(row.type)}</TableCell>
                  <TableCell>{row.value.toLocaleString('pt-br', { minimumFractionDigits: 2 })}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </CardContent>
    </Card>
  );
});

export default Extract;
