import makeStyles from '@material-ui/core/styles/makeStyles';
import Drawer from 'components/Layout/Drawer';
import ViewDashboardIcon from 'mdi-react/ViewDashboardIcon';
import CashPlusIcon from 'mdi-react/CashPlusIcon';
import WalletOutlineIcon from 'mdi-react/WalletOutlineIcon';
import CartOutlineIcon from 'mdi-react/CartOutlineIcon';
import ClipboardListIcon from 'mdi-react/ClipboardListIcon';

import React, { memo, useCallback, useRef, useState } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import DashboardIndexPage from './Dashboard';
import DepositIndexPage from './Deposit';
import PurchaseIndexPage from './Purchase';
import SellIndexPage from './Sell';
import ExtractIndexPage from './Extract';

export const ScrollTopContext = React.createContext<Function>(() => {});

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    display: 'flex',
    width: '100vw',
    height: '100vh'
  },
  content: {
    backgroundColor: theme.palette.background.default,
    width: '100vw',
    height: '100vh',
    overflow: 'auto',
    padding: theme.variables.contentPadding,
    [theme.breakpoints.up('sm')]: {
      padding: theme.variables.contentPaddingUpSm
    }
  }
}));

const AdminPage = memo((props: {}) => {
  const classes = useStyles(props);

  const mainContent = useRef<HTMLDivElement>();
  const [menu] = useState([
    { path: '/', display: 'Dashboard', icon: ViewDashboardIcon },
    {
      path: '/deposito',
      display: 'Deposito',
      icon: CashPlusIcon
    },
    { path: '/compra', display: 'Compra', icon: WalletOutlineIcon },
    { path: '/venda', display: 'Venda', icon: CartOutlineIcon },
    { path: '/extrato', display: 'Extrato', icon: ClipboardListIcon }
  ]);

  const scrollTop = useCallback(() => setTimeout(() => mainContent.current.scrollTo(0, 0), 100), []);
  const renderRedirect = useCallback(() => <Redirect to='/' />, []);

  return (
    <div className={classes.root}>
      <ScrollTopContext.Provider value={scrollTop}>
        <Drawer menu={menu}>
          <main ref={mainContent} className={classes.content}>
            <Switch>
              <Route path='/deposito' component={DepositIndexPage} />
              <Route path='/compra' component={PurchaseIndexPage} />
              <Route path='/venda' component={SellIndexPage} />
              <Route path='/extrato' component={ExtractIndexPage} />
              <Route path='/' component={DashboardIndexPage} />
              <Route render={renderRedirect} />
            </Switch>
          </main>
        </Drawer>
      </ScrollTopContext.Provider>
    </div>
  );
});

export default AdminPage;
