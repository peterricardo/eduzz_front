import React, { memo, Fragment } from 'react';
import Toolbar from 'components/Layout/Toolbar';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import LinearProgress from '@material-ui/core/LinearProgress';
import { useFormikObservable } from 'hooks/useFormikObservable';
import Grid from '@material-ui/core/Grid';
import TextField from 'components/Shared/Fields/Text';
import Button from '@material-ui/core/Button';
import Toast from 'components/Shared/Toast';
import Typography from '@material-ui/core/Typography';

import * as yup from 'yup';
import bitcoinService from 'services/bitcoin';
import { tap } from 'rxjs/operators';
import { logError } from 'helpers/rxjs-operators/logError';

const validationSchema = yup.object().shape({
  amount: yup.number().required().min(1)
});

const SellPage = memo(() => {
  const formik = useFormikObservable({
    initialValues: { amount: 0 },
    validationSchema,
    onSubmit(model) {
      return bitcoinService.sell(model).pipe(
        tap(() => {
          Toast.show('Venda realizada com sucesso!');
          formik.resetForm();
        }),
        logError(true)
      );
    }
  });

  return (
    <Fragment>
      <Toolbar title='Venda' />
      <Typography gutterBottom variant='h4'>
        Venda
      </Typography>
      <Card>
        {formik.isSubmitting && <LinearProgress color='primary' />}
        <CardContent>
          <form onSubmit={formik.handleSubmit}>
            <Grid item xs={12} sm={6}>
              <TextField label='Valor' name='amount' mask='money' formik={formik} />
            </Grid>
            <Button color='primary' variant='contained' type='submit' disabled={formik.isSubmitting}>
              Vender
            </Button>
          </form>
        </CardContent>
      </Card>
    </Fragment>
  );
});

export default SellPage;
