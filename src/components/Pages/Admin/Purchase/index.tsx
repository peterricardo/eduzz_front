import React, { memo, Fragment } from 'react';
import Toolbar from 'components/Layout/Toolbar';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import LinearProgress from '@material-ui/core/LinearProgress';
import makeStyles from '@material-ui/core/styles/makeStyles';
import BitcoinIcon from 'mdi-react/BitcoinIcon';

import { useFormikObservable } from 'hooks/useFormikObservable';
import Grid from '@material-ui/core/Grid';
import TextField from 'components/Shared/Fields/Text';
import Button from '@material-ui/core/Button';
import Toast from 'components/Shared/Toast';
import Balance from 'components/Shared/Balance';
import BitcoinQuotation from 'components/Shared/BitcoinQuotation';
import Typography from '@material-ui/core/Typography';

import { useObservable } from 'react-use-observable';

import * as yup from 'yup';
import bitcoinService from 'services/bitcoin';
import { tap } from 'rxjs/operators';
import { logError } from 'helpers/rxjs-operators/logError';

const validationSchema = yup.object().shape({
  amount: yup.number().required().min(1)
});

const useStyle = makeStyles({
  card: {
    marginTop: 15
  },
  fullWidth: {
    width: '100%'
  }
});

const PurchasePage = memo(props => {
  const classes = useStyle(props);

  // LOAD CURRENT BITCOIN PRICE
  const [data] = useObservable(() => {
    return bitcoinService.quotation().pipe(logError());
  }, []);

  const { buy } = data || ({ buy: 0 } as typeof data);

  const formik = useFormikObservable({
    initialValues: { amount: 0 },
    validationSchema,
    onSubmit(model) {
      return bitcoinService.purchase(model).pipe(
        tap(() => {
          Toast.show('Compra realizada com sucesso!');
          formik.resetForm();
        }),
        logError(true)
      );
    }
  });

  const calculateBitcoin = (e: any) => {
    const value = e.target.value.replace(/\D+/g, '').toString();

    const valueConverted = parseFloat(`${value.substring(0, value.length - 2)}.${value.substring(value.length - 2)}`);
    formik.setFieldValue('btc', valueConverted / buy);
  };

  return (
    <Fragment>
      <Toolbar title='Compra' />
      <Typography gutterBottom variant='h4'>
        Compra
      </Typography>
      <Grid container spacing={3}>
        <Grid container item lg={6}>
          {!formik.isSubmitting && <Balance />}
        </Grid>
        <Grid container item lg={6}>
          {!formik.isSubmitting && <BitcoinQuotation />}
        </Grid>
      </Grid>
      <Card className={classes.card}>
        {formik.isSubmitting && <LinearProgress color='primary' />}
        <CardContent>
          <form onSubmit={formik.handleSubmit} onChange={calculateBitcoin}>
            <Grid container spacing={2} alignItems='center'>
              <Grid container item lg={3}>
                <TextField label='Valor' name='amount' mask='money' formik={formik} className={classes.fullWidth} />
              </Grid>
              <BitcoinIcon />
              <Grid container item lg={3}>
                <TextField
                  label='Valor Estimado Btc'
                  name='btc'
                  formik={formik}
                  disabled
                  className={classes.fullWidth}
                />
              </Grid>
            </Grid>
            <Button color='primary' variant='contained' type='submit' disabled={formik.isSubmitting}>
              Comprar
            </Button>
          </form>
        </CardContent>
      </Card>
    </Fragment>
  );
});

export default PurchasePage;
