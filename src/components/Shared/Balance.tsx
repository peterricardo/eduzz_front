import React, { memo } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import makeStyles from '@material-ui/core/styles/makeStyles';

import { logError } from 'helpers/rxjs-operators/logError';
import { useObservable } from 'react-use-observable';
import userService from 'services/user';

const useStyles = makeStyles({
  card: {
    padding: 15,
    width: '100%'
  }
});

const Balance = memo((props: {}) => {
  const classes = useStyles(props);
  const [data] = useObservable(() => {
    return userService.balance().pipe(logError());
  }, []);

  const { balance } = data || ({ balance: 0 } as typeof data);

  return (
    <Card className={classes.card}>
      <Typography gutterBottom variant='h4'>
        Saldo
      </Typography>
      <CardContent>
        <Typography gutterBottom variant='h3'>
          R$ {balance.toLocaleString('pt-br', { minimumFractionDigits: 2 })}
        </Typography>
      </CardContent>
    </Card>
  );
});

export default Balance;
