import React, { memo } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import makeStyles from '@material-ui/core/styles/makeStyles';

import { logError } from 'helpers/rxjs-operators/logError';
import { useObservable } from 'react-use-observable';
import bitcoinService from 'services/bitcoin';

const useStyles = makeStyles({
  card: {
    padding: 15,
    width: '100%'
  },
  root: {
    display: 'flex',
    flexDirection: 'row'
  }
});

const Balance = memo((props: {}) => {
  const classes = useStyles(props);
  const [data] = useObservable(() => {
    return bitcoinService.volume().pipe(logError());
  }, []);

  const { buy, sell } = data || ({ buy: 0, sell: 0 } as typeof data);

  return (
    <Card className={classes.card}>
      <Typography gutterBottom variant='h4'>
        Volume do Dia
      </Typography>
      <CardContent className={classes.root}>
        <Box flex={1}>
          <Typography gutterBottom variant='h6'>
            Compra
          </Typography>
          <Typography gutterBottom variant='h5'>
            R$ {buy.toLocaleString('pt-br', { minimumFractionDigits: 5 })}
          </Typography>
        </Box>
        <Box flex={1}>
          <Typography gutterBottom variant='h6'>
            Venda
          </Typography>
          <Typography gutterBottom variant='h5'>
            R$ {sell.toLocaleString('pt-br', { minimumFractionDigits: 5 })}
          </Typography>
        </Box>
      </CardContent>
    </Card>
  );
});

export default Balance;
